/* Global */
var lastClickedValue = 0;
var initialCollection = null;

$(function () {

    $('#loader').off();
    $('#loader').modal('show');

    $.ajax({
        url: 'http://localhost:3000/product',
        beforeSend: function (xhr) {

        }, success: function (data) {
            initialCollection = data;

            /* Fill table */
            var html = '';
            for (var key = 0, size = data.length; key < size; key++) {
                if (data[key].meta_key == '_regular_price') {
                    html += '<tr><th scope"row">'
                        + data[key].ID
                        + '</td><td>'
                        + data[key].post_title
                        + '</td><td>'
                        + '<div class="input-group mb-3">'
                        + '<span class="input-group-text">$</span>'
                        + '<input type="text" class="form-control product-price" value="' + data[key].meta_value + '" aria-label="Amount">'
                        + '</div>'
                        + '</td><td>'
                        + 'No disponible'
                        + '</td>'
                        + '</tr>';
                }
            }

            $('#table-content').append(html);
            $('#loader').modal('hide');

            let table = $('#products-table').DataTable({ paging: false, ordering: false, searching: true, bInfo: false, "dom": '<"top"i>rt<"bottom"><"clear">' });

            document.querySelector('#search-button').addEventListener('click', (event) => {
                let value = $('#search-input').val();
                console.log(value);
                console.log(table);
                table.search(value).draw();
            });

            document.querySelector('#search-input').addEventListener('keypress', (e) => {
                if (e.key === 'Enter') {
                    let value = $('#search-input').val();
                    console.log(value);
                    console.log(table);
                    table.search(value).draw();
                }
            });

            document.querySelectorAll('.product-price').forEach(item => {

                item.addEventListener('click', (event) => {
                    let value = event.target.value;
                    lastClickedValue = value;
                    console.log('Nuevo valor almacenado: ' + value);
                });

                item.addEventListener('change', (event) => {
                    let value = event.target.value;

                    if (isValidValue(value)) {
                        let id = event.target.parentElement.parentElement.parentElement.querySelector('th').outerText;

                        /* Post */
                        $.ajax({
                            type: "POST",
                            url: 'http://localhost:3000/product/' + id,
                            data: { productPrice: value },
                            success: () => {
                                console.log('El elemento con ID: ' + id + ' fue modificado correctamente con el valor ' + value + '.');
                            },
                            error: () => {
                                console.error('El elemento con ID: ' + id + ' no pudo ser modificado con el valor ' + value + '.');
                            },
                        });

                    } else {
                            alert('El valor ingresado no es valido.');
                            
                        event.target.value = lastClickedValue;
                    }

                });
            });

        }, error: function (data) {
            alert('No es posible conectar a la base de datos, contacte al desarrollador.');
            $('#loader').modal('hide');
        }
    })
});

/* Validations */
function isValidValue($value) {
    if (!isNaN($value))
        if ($value > 0)
            return true;
    return false;
}