const { constants } = require('fs');
let express = require('express');
let app = express();
let connection = require('./config/database.js');
const { resolveSoa } = require('dns');
const { post } = require('jquery');
const bodyParser = require('body-parser');

/* Start running */
app.use(bodyParser.urlencoded({ extended: true }));
app.listen(3000, () => {
    console.log('App running on port 3000');
});

/* Products */
app.get('/product', (req, res) => {
    connection.query("SELECT  wp_posts.ID, wp_posts.post_title, wp_posts.post_status, wp_postmeta.meta_id, wp_postmeta.meta_key, wp_postmeta.meta_value FROM wp_posts INNER JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id", function (error, results, fields) {
        if (error) console.log(error);
        res.json(results);
    });
});

app.post('/product/:productId', (req, res) => {
    /* Expected parameters */
    let price = req.body.productPrice;
    let postId = req.params.productId;

    let query = "UPDATE wp_postmeta SET meta_value = '" + price + "' WHERE post_id = '" + postId + "' AND (meta_key = '_price' OR meta_key = '_regular_price')";
    console.log(query);

    connection.query(query, function (error, results, fields) {
        if (error) throw error;
        res.json(results);
    });
});