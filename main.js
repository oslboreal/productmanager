const { app, BrowserWindow } = require("electron");

let mainWindow;

function createWindow() {
    // Node js server is going to run here.
    let server = require('./server/server.js');

    mainWindow = new BrowserWindow({
        width: 500,
        height: 500
    })

    mainWindow.maximize();
    mainWindow.loadFile('./electron/index.html');
}

app.on('ready', createWindow);
